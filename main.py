#!/usr/bin/env python
import webapp2
import jinja2
import json
from google.appengine.ext import db
from google.appengine.api import users
from models import Contact

TEMPLATE_DIR = 'templates'


jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(TEMPLATE_DIR))

class Handler(webapp2.RequestHandler):
	def write(self, *args, **kwargs):
		self.response.out.write(*args, **kwargs)

	def render(self, template, **kwargs):
		temp = jinja_env.get_template(template)
		render_str = temp.render(**kwargs)
		self.write(render_str)

class Main(Handler):
	def get(self):
		self.render('index.html', contacts=Contact.all())

class Create(Handler):
	def post(self):
		data = self.request.body
		data = json.loads(data)
		Contact(first_name=data['first_name'], last_name=data['last_name'], zipcode=data['zipcode'], state=data['state'], city=data['city']).put()
		self.render('index.html', contacts=Contact.all())
		
class Edit(Handler):
	def post(self):
		data = self.request.body
		data = json.loads(data)
		db.delete(db.GqlQuery("SELECT * FROM Contact WHERE first_name = :1 AND last_name <= :2 AND zipcode = :3",data['old_first'], data['old_last'], data['old_zip']))
		Contact(first_name=data['first_name'], last_name=data['last_name'], zipcode=data['zipcode'], state=data['state'], city=data['city']).put()
		self.render('index.html', contacts=Contact.all())
		
class Delete(Handler):
	def post(self):
		data = self.request.body
		data = json.loads(data)
		db.delete(db.GqlQuery("SELECT * FROM Contact WHERE first_name = :1 AND last_name <= :2 AND zipcode = :3",data['old_first'], data['old_last'], data['old_zip']))
		self.render('index.html', contacts=Contact.all())

		
app = webapp2.WSGIApplication([('/', Main),
							   ('/create_user', Create),
							   ('/edit_user', Edit),
							   ('/delete_user', Delete)], debug=True)