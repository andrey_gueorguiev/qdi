#!/usr/bin/env python

from google.appengine.ext import db


class Contact(db.Model):
	first_name = db.StringProperty(required=True)
	last_name = db.StringProperty(required=True)
	zipcode = db.StringProperty(required=True)
	state = db.StringProperty(required=True)
	city = db.StringProperty(required=True)
	created = db.DateTimeProperty(auto_now_add=True)
